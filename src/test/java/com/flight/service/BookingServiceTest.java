package com.flight.service;

import java.util.Date;
import java.util.Optional;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.flight.dto.BookingDto;
import com.flight.exception.BookingNotfoundException;
import com.flight.exception.DataNotFound;
import com.flight.exception.SeatsNotAvilableException;
import com.flight.exception.UserNotfoundException;
import com.flight.model.Booking;
import com.flight.model.Flight;
import com.flight.model.User;
import com.flight.repository.BookingRepo;
import com.flight.repository.FlightRepo;
import com.flight.repository.UserRepo;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingServiceTest {
	@InjectMocks
	FlightBookingServiceImpl flightBookingService;
	@Mock
	BookingRepo bookingRepo;
	@Mock
	RestTemplate restTemplate;
	
	@Mock
	FlightRepo flightRepo;
	@Mock
	UserRepo userRepo;
	@Test(expected = UserNotfoundException.class)
	public void bookBusServicefrusr() {
		BookingDto bookingDto = new BookingDto();
		Booking booking =new Booking();
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(null);
		flightBookingService.makeBooking(bookingDto);
	}
	
	@Test(expected = DataNotFound.class)
	public void bookBusServiceTestbus() {
		BookingDto bookingDto = new BookingDto();
		Booking booking =new Booking();
		User usr =new User();
		usr.setEmail("sk");
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
		Assert.assertNotNull(usr);
		Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto));
		
	}
	
	@Test(expected = SeatsNotAvilableException.class)
	public void bookBusServicef() {
		BookingDto bookingDto = new BookingDto();
		
		bookingDto.setNoOfSeatsBooked(2);
		bookingDto.setFlightId(1);
		Booking booking =new Booking();
		User usr =new User();
		BeanUtils.copyProperties(bookingDto, booking);
		Flight b = new Flight();
		b.setFare(100.0);
		Optional<Flight> bus = Optional.ofNullable(new Flight());
		//bus.setBusId(1);
		
		usr.setEmail("sk");
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
		Mockito.when(flightRepo.findById(1)).thenReturn(bus);
		flightRepo.save(b);
		booking.setAmount(b.getFare()*bookingDto.getNoOfSeatsBooked());
		booking.setUser(usr);
		booking.setFlight(b);
		
		Assert.assertNotNull(usr);
		Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto));
		
	}
	
	/*@Test()
	public void bookBusServicefkk() {
		BookingDto bookingDto = new BookingDto();
		
		bookingDto.setNoOfSeatsBooked(2);
		bookingDto.setFlightId(1);
		Booking booking =new Booking();
		User usr =new User();
		BeanUtils.copyProperties(bookingDto, booking);
		Flight b = new Flight();
		b.setFare(100.0);
		b.setNoOfSeatsAvailable(44);
		Optional<Flight> bus = Optional.ofNullable(new Flight());
		if(b.getNoOfSeatsAvailable() > bookingDto.getNoOfSeatsBooked()) {
			return true;
		}
		usr.setEmail("sk");
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
		Mockito.when(flightRepo.findById(1)).thenReturn(bus);
		flightRepo.save(b);
		booking.setAmount(b.getFare()*bookingDto.getNoOfSeatsBooked());
		booking.setUser(usr);
		booking.setFlight(b);
		
		Assert.assertNotNull(usr);
		Assert.assertEquals(booking, flightBookingService.makeBooking(bookingDto));
		
	}*/
	
	
	
	
	@Test
	public void booResponseEntity() {
		BookingDto bookingDto = new BookingDto();
		
		
		Assert.assertEquals(null,flightBookingService.transction(bookingDto, 2));
		
	}	
	
	
	
	
	@Test
	public void booResponseEntitypos() {
		BookingDto bookingDto = new BookingDto();

		String uri = "http://BANKING-SERVICE/transaction";
		double sum =5;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("accountNumber", bookingDto.getAccountNumber());
		request.put("amount", sum);
		request.put("banificiaryAccountNo", 6666);

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);
		Assert.assertEquals(null,flightBookingService.transction(bookingDto, 5));
		
	}	
	
	@Test
	public void testFindByIdForNegative() {
		Booking booking = new Booking(-1, new Date(2020 - 04 - 15), 2, 3000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(-1)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(-1);
		Assert.assertNotNull(book);
		Assert.assertEquals(2, book.getNoOfSeatsBooked());
	}

	@Test
	public void testFindByIdForPositive() {
		Booking booking = new Booking(11, new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(11)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(11);
		Assert.assertNotNull(book);
		Assert.assertEquals(3, book.getNoOfSeatsBooked());
	}

	@Test
	public void testFindByIdForPositiveDate() {
		Booking booking = new Booking(11, new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(11)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(11);
		Assert.assertNotNull(book);
		Assert.assertEquals(new Date(2020 - 04 - 15), book.getBookingDate());
	}

	@Test(expected = BookingNotfoundException.class)
	public void testFindByIdForException() throws BookingNotfoundException {
		Booking booking = new Booking(11, new Date(2020 - 04 - 15), 3, 9000.00, new Date(2020 - 04 - 20));
		Mockito.when(bookingRepo.findById(11)).thenReturn(Optional.of(booking));
		Booking book = flightBookingService.getBookingById(14);
		Assert.assertNotNull(book);
		Assert.assertEquals(4, book.getNoOfSeatsBooked());
	}
	
	
	
	
	
	

}
