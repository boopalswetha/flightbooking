package com.flight.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.model.User;



public interface UserRepo extends JpaRepository<User, Integer> {
	User findByEmail(String email);
	User findByEmailAndPassword(String email, String password);
}
