package com.flight.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.flight.model.Booking;
@Repository
public interface BookingRepo extends JpaRepository<Booking, Integer> {
	

}
