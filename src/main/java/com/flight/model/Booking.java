package com.flight.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "booking")
public class Booking {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int bookingId;
	@Temporal(TemporalType.DATE)
	private Date bookingDate;
	private int noOfSeatsBooked;
	private Double amount;
	@Temporal(TemporalType.DATE)
	private Date journeyDate;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="flightId")
	private Flight flight;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	
	@JoinColumn(name = "userId")
	private User user;

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public int getNoOfSeatsBooked() {
		return noOfSeatsBooked;
	}

	public void setNoOfSeatsBooked(int noOfSeatsBooked) {
		this.noOfSeatsBooked = noOfSeatsBooked;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(Date journeyDate) {
		this.journeyDate = journeyDate;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Booking(int bookingId, Date bookingDate, int noOfSeatsBooked, Double amount, Date journeyDate, Flight flight,
			User user) {
		super();
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.noOfSeatsBooked = noOfSeatsBooked;
		this.amount = amount;
		this.journeyDate = journeyDate;
		this.flight = flight;
		this.user = user;
	}

	public Booking(int bookingId, Date bookingDate, int noOfSeatsBooked, Double amount, Date journeyDate) {
		super();
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.noOfSeatsBooked = noOfSeatsBooked;
		this.amount = amount;
		this.journeyDate = journeyDate;
	}

	public Booking() {
		
	}

	
	
}
