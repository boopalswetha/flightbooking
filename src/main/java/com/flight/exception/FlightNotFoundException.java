package com.flight.exception;

public class FlightNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FlightNotFoundException(String source) {
		super(String.format(" %s ", source));
	}

}
