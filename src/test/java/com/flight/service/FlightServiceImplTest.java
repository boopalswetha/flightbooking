package com.flight.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.flight.dto.FlightDto;
import com.flight.exception.FlightNotFoundException;
import com.flight.model.Flight;
import com.flight.repository.FlightRepo;


@RunWith(MockitoJUnitRunner.Silent.class)
public class FlightServiceImplTest {
	@InjectMocks
	FlightServiceImpl flightServiceImpl;

	@Mock
	FlightRepo flightRepository;

	static Flight flight = null;

	@BeforeClass
	public static void setUp() {
		flight = new Flight();
	}

	@Test(expected = FlightNotFoundException.class)
	public void testsearchbusForPositive() {
		List<Flight> flights = new ArrayList<Flight>();
		Flight flight = new Flight();
		FlightDto flightDto = new FlightDto();
		flight.setFlightId(1);
		flight.setDestination("bangalore");
		flight.setSource("salem");
		flights.add(flight);
		Mockito.when(flightRepository.findFlightBySourceAndDestinationAndJourneyDate("salem", "bangalore", null))
				.thenReturn(flights);
		List<Flight> flightss = flightServiceImpl.searchFlight(flightDto);
		Assert.assertNotNull(flightss);
		Assert.assertEquals(1, flightss.size());

	}

	@Test
	public void testsearchFlightForNagative() {
		List<Flight> flights = new ArrayList<Flight>();
		Flight flight = new Flight();
		flight.setFlightId(-1);
		FlightDto flightDto = new FlightDto();
		flightDto.setDestination("bangalore");
		flightDto.setSource("salem");
		flightDto.setJourneyDate(null);
		flights.add(flight);
		Mockito.when(flightRepository.findFlightBySourceAndDestinationAndJourneyDate("salem", "bangalore", null))
				.thenReturn(flights);
		List<Flight> flightss = flightServiceImpl.searchFlight(flightDto);
		Assert.assertNotNull(flightss);
		Assert.assertEquals(1, flightss.size());

	}
	
	@Test(expected = FlightNotFoundException.class)
	public void testsearchbusForException() throws FlightNotFoundException {
		List<Flight> flights = new ArrayList<Flight>();
		Flight flight = new Flight();
		FlightDto flightDto = new FlightDto();
		flight.setFlightId(1);
		flight.setDestination("bangalore");
		flight.setSource("salem");
		flights.add(flight);
		Mockito.when(flightRepository.findFlightBySourceAndDestinationAndJourneyDate("salem", "bangalore", null))
				.thenReturn(flights);
		List<Flight> flightss = flightServiceImpl.searchFlight(flightDto);
		Assert.assertNotNull(flightss);
		Assert.assertEquals(2, flightss.size());

	}

	@AfterClass
	public static void tearDown() {
		flight = null;
	}
}
