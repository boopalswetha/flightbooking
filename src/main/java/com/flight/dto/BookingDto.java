package com.flight.dto;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class BookingDto {
	public BookingDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Temporal(TemporalType.DATE)
	private Date bookingDate;
	private int noOfSeatsBooked;
	private int accountNumber;
	
	private String emailId;
	private int flightId;
	
	public Date getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}
	public int getNoOfSeatsBooked() {
		return noOfSeatsBooked;
	}
	public void setNoOfSeatsBooked(int noOfSeatsBooked) {
		this.noOfSeatsBooked = noOfSeatsBooked;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	public int getFlightId() {
		return flightId;
	}
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	
	
	
	
}
